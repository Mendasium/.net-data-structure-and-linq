﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model.RawModel
{
    class RawUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }

        [JsonProperty(PropertyName = "First_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "Last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "Registered_at")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty(PropertyName = "Team_id")]
        public int? TeamId { get; set; }

        public override string ToString()
        {
            return $"{Id} - {FirstName} {LastName}";
        }
    }
}
