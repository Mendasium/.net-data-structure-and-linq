﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model.RawModel
{
    class RawProject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        [JsonProperty(PropertyName = "Created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(PropertyName = "Author_id")]
        public int AuthorId { get; set; }
        [JsonProperty(PropertyName = "Team_id")]
        public int TeamId { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Name} - {Description} - {CreatedAt.Date}";
        }
    }
}
