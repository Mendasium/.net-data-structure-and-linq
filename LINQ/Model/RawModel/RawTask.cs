﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model.RawModel
{
    class RawTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonProperty(PropertyName = "Created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(PropertyName = "Finished_at")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty(PropertyName = "State_id")]
        public int StateId { get; set; }
        [JsonProperty(PropertyName = "Project_id")]
        public int ProjectId { get; set; }
        [JsonProperty(PropertyName = "Performer_id")]
        public int PerformerId { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Name} - {Description}";
        }
    }
}
