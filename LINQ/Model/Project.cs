﻿using LINQ.Model.RawModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

        public Project() { }

        public Project(RawProject project)
        {
            Id = project.Id;
            Name = project.Name;
            Description = project.Description;
            CreatedAt = project.CreatedAt;
            Deadline = project.Deadline;
        }

        public override string ToString()
        {
            string author = Author == null ? "" : "\n\tAuthor: " + Author.ToString();
            string team = Team == null ? "" : "\n\tTeam: " + Team.ToString();
            return $"{Id} - {Name} - {Description} {team} {author}";
        }
    }
}
