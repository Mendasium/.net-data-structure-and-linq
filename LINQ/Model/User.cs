﻿using LINQ.Model.RawModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }

        public User() { }

        public User(RawUser user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Birthday = user.Birthday;
            RegisteredAt = user.RegisteredAt;
        }
        
        public override string ToString()
        {
            string team = Team == null ? "" : "\n\tTeam: " + Team.ToString();
            return $"{Id} - {FirstName} {LastName}  {team}";
        }
    }
}
