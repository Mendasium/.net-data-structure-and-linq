﻿using LINQ.Model.RawModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public State State { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }

        public Task() { }

        public Task(RawTask task)
        {
            Id = task.Id;
            Name = task.Name;
            Description = task.Description;
            CreatedAt = task.CreatedAt;
            FinishedAt = task.FinishedAt;
        }

        public override string ToString()
        {
            string proj = Project == null ? "" : ("\n\tProject: " + Project.ToString());
            string state = State == null ? "" : ("\n\tState: " + State.ToString());
            string perf = Performer == null ? "" : ("\n\tPerformer: " + Performer.ToString());
            return $"{Id} - {Name} - {Description} {proj} {state}";
        }
    }
}
