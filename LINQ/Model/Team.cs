﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Created_at")]
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Name} - {CreatedAt.Date}";
        }
    }
}
