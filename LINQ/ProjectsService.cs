﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using LINQ.Model;
using Newtonsoft.Json;
using LINQ.Model.RawModel;

namespace LINQ
{
    class ProjectsService
    {
        private static HttpClient Client = new HttpClient();
        //Set URL here
        private const string url = "https://bsa2019.azurewebsites.net/api/";

        //Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        //Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2019) году для конкретного пользователя (по id).
        //


        public User GetUser(int id)
        {
            RawUser rawUser = GetUserByIdAsync(id).Result;
            var user = new User(rawUser);
            if (rawUser.TeamId != null)
                user.Team = GetTeamByIdAsync((int)rawUser.TeamId).Result;
            return user;
        }

        public Project GetProject(int id)
        {
            RawProject rawProject = GetProjectByIdAsync(id).Result;
            var project = new Project(rawProject);
            project.Author = GetUser(rawProject.AuthorId);
            project.Team = GetTeamByIdAsync(rawProject.TeamId).Result;
            return project;
        }

        public async Task<RawUser> GetUserByIdAsync(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Users/" + id);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RawUser>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting a User, the error was caught:\n {e.Message}");
                return new RawUser();
            }
        }

        public async Task<Team> GetTeamByIdAsync(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Teams/" + id);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Team>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting a Team, the error was caught:\n {e.Message}");
                return new Team();
            }
        }

        public async Task<RawProject> GetProjectByIdAsync(int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Projects/" + id);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<RawProject>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting a Project, the error was caught:\n {e.Message}");
                return new RawProject();
            }
        }


        public async Task<IEnumerable<RawUser>> GetUsersAsync()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Users");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<RawUser>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting Users, the error was caught:\n {e.Message}");
                return new RawUser[0];
            }
        }

        public async Task<IEnumerable<Team>> GetTeamsAsync()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Teams");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Team>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting Teams, the error was caught:\n {e.Message}");
                return new Team[0];
            }
        }

        public async Task<IEnumerable<State>> GetTaskStatesAsync()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "TaskStates/");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<State>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting TaskStates, the error was caught:\n {e.Message}");
                return new State[0];
            }
        }

        public async Task<IEnumerable<RawTask>> GetTasksAsync()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Tasks");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<RawTask>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting Tasks, the error was caught:\n {e.Message}");
                return new RawTask[0];
            }
        }

        public async Task<IEnumerable<RawProject>> GetProjectsAsync()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url + "Projects");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<RawProject>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine($"While getting Projects, the error was caught:\n {e.Message}");
                return new RawProject[0];
            }
        }
    }
}
