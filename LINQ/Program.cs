﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        private static ProjectInterface projectInterface;
        static void Main(string[] args)
        {
            projectInterface = new ProjectInterface();

            projectInterface.Start();
        }
    }
}
