﻿using LINQ.Model;
using LINQ.Model.RawModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQ
{
    class ProjectInterface
    {
        string[] menu;
        ProjectsService projectsService;
        readonly State[] states;

        public ProjectInterface()
        {
            projectsService = new ProjectsService();
            states = projectsService.GetTaskStatesAsync().Result.ToArray();
            menu = new string[]
            {
                "Show projects and its tasksCount by UserId",
                "Show tasks with name shorter then 45 symbols by UserId",
                "Show (id,name) tasks, that was finished this year by UserId",
                "Show list of teams, all members of which are older than 12 years, sorted by registrationDate and grouped by teams",
                "Show list of users, sorted alphabetically with sorted tasks by name length",
                "Show structure, connected with user and his project, by its id",
                "Show structure, connected with Project and its tasks, by project id"
            };
        }

        public void Start()
        {
            int result = 0;
            while (true)
            {
                result = GetMenuItem();
                switch (result)
                {
                    case 1:
                        ShowProjectTasksCountByUserId();
                        break;
                    case 2:
                        ShowShortnameTasksByPerformerIdByUserId();
                        break;
                    case 3:
                        ShowFinishedTasksByUserId();
                        break;
                    case 4:
                        ShowSortedTeams();
                        break;
                    case 5:
                        ShowSortedUsersWithTasks();
                        break;
                    case 6:
                        ShowUserInfoById();
                        break;
                    case 7:
                        ShowProjectInfoById();
                        break;
                    default:
                        TypeInfo("Enter correct menu option");
                        break;
                }
            }
        }

        private int GetId()
        {
            int res = -1;
            while (res == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out res) || res <= 0)
                {
                    TypeInfo("Enter correct id, please");
                    res = -1;
                    continue;
                }
            }
            return res;
        }

        private int GetMenuItem()
        {
            for (int i = 1; i <= menu.Length; i++)
            {
                TypeInfo(i + " - " + menu[i - 1]);
            }
            Console.WriteLine();
            string result = "";
            int res = -1;
            while (res < 0)
            {
                result = Console.ReadLine();
                if (!int.TryParse(result, out res) || (res > 7 || res <= 0))
                {
                    res = -1;
                    TypeInfo("Please enter correct number");
                }
            }
            return res;
        }

        private void TypeInfo(string info)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(info);
            Console.ResetColor();
        }




        #region ProjectTasksCountByUserId
        private void ShowProjectTasksCountByUserId()
        {
            TypeInfo("Enter UserId");
            var res = GetProjectTasksCountByUserId(GetId());
            if (res.Keys.Count == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result");
            foreach (var pair in res)
            {
                TypeInfo("Project:");
                Console.WriteLine("\t" + pair.Key);
                Console.WriteLine("Tasks count: " + pair.Value);
            }
        }

        //Получить кол-во тасков у проекта конкретного пользователя(по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        public Dictionary<Project, int> GetProjectTasksCountByUserId(int id)
        {
            var tasks = projectsService.GetTasksAsync().Result;
            var projects = projectsService.GetProjectsAsync().Result;

            return tasks.Where(t => t.PerformerId == id)
                .Join(projects, x => x.ProjectId, y => y.Id, (x, y) => new Task(x) { Project = new Project(y) })
                .GroupBy(x => x.Project)
                .ToDictionary(x => x.Key, x => x.Count());
        }
        #endregion

        #region ShortnameTasksByPerformerIdByUserId
        private void ShowShortnameTasksByPerformerIdByUserId()
        {
            TypeInfo("Enter UserId");
            var res = GetShortnameTasksByPerformerIdByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var el in res)
            {
                Console.WriteLine("\t" + el);
            }
        }

        //Получить список тасков, назначенных на конкретного пользователя(по id), где name таска < 45 символов (коллекция из тасков).
        public IEnumerable<Task> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            var projects = projectsService.GetTasksAsync().Result;
            var users = projectsService.GetUsersAsync().Result;

            return projects.Where(t => t.Name.Length < 45 && t.PerformerId == id)
                .Select(x =>
                {
                    return new Task(x)
                    {
                        Performer = new User(users.FirstOrDefault(y => x.PerformerId == y.Id)),
                        State = states.FirstOrDefault(s => s.Id == x.StateId)
                    };
                });
        }
        #endregion

        #region FinishedTasksByUserId
        private void ShowFinishedTasksByUserId()
        {
            TypeInfo("Enter UserId");
            var res = GetFinishedTasksByUserId(GetId());
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (Id, Name) in res)
            {
                Console.WriteLine("\tProject" + Id + " - " + Name);
            }
        }

        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2019) году для конкретного пользователя (по id).
        public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            var projects = projectsService.GetTasksAsync().Result;

            return projects.Where(t => t.PerformerId == id && t.FinishedAt.Year == year)
                .Select(x =>
                {
                    return new Task(x)
                    {
                        State = states.FirstOrDefault(s => s.Id == x.StateId)
                    };
                }).Where(t => t.State.Value.Equals("finished"))
                .Select(x => (Id: x.Id, Name: x.Name));
        }
        #endregion

        #region SortedTeams
        private void ShowSortedTeams()
        {
            var res = GetSortedTeams();
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (id, name, userList) in res)
            {
                Console.WriteLine("\tProject:" + id + " - " + name);
                TypeInfo("\tUsers:");
                foreach (var u in userList)
                    Console.WriteLine("\t\t" + u.ToString());
            }
        }

        //Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 12 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
        public IEnumerable<(int id, string name, List<User> userList)> GetSortedTeams()
        {//Команд в которых все учасники старше 12 лет нету
            var users = projectsService.GetUsersAsync().Result;
            var teams = projectsService.GetTeamsAsync().Result;

            return users.Join(teams, x => x.TeamId, y => y.Id, (x, y) => new User(x) { Team = y })
                .GroupBy(u => u.Team.Id)
                .Where(u => u.All(x => DateTime.Now.Year - x.Birthday.Year >= 12))
                .Select(t =>
                    (id: t.Key, 
                    name: teams.FirstOrDefault(x => x.Id == t.Key).Name, 
                    userList: t.OrderByDescending(x => x.RegisteredAt)
                                .ToList())
                    );
        }
        #endregion

        #region SortedUsersWithTasks
        private void ShowSortedUsersWithTasks()
        {
            var res = GetSortedUsersWithTasks();
            if (res.Count() == 0)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            foreach (var (user, tasks) in res)
            {
                TypeInfo("\tUser:");
                Console.WriteLine("\t\t" + user);
                TypeInfo("\tTasks:");
                foreach (var t in tasks)
                    Console.WriteLine("\t\t\t" + t.ToString());
            }
        }

        //Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public IEnumerable<(User user, List<Task> tasks)> GetSortedUsersWithTasks()
        {
            var tasks = projectsService.GetTasksAsync().Result;
            var users = projectsService.GetUsersAsync().Result;

            return tasks.Join(users, x => x.PerformerId, y => y.Id, (x, y) => new Task(x) { Performer = new User(y) })
                .GroupBy(x => x.Performer.Id)
                .Select(x => (Performer: x.FirstOrDefault().Performer, Tasks: x.OrderByDescending(y => y.Name.Length).ToList()))
                .OrderBy(x => x.Performer.FirstName);
        }
        #endregion

        #region UserInfoById
        private void ShowUserInfoById()
        {
            TypeInfo("Enter UserId");
            var res = GetUserInfoById(GetId());
            if (res.User == null)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            Console.WriteLine("\tUser: " + res.User);
            Console.WriteLine("\tLastProject: " + res.LastProject);
            Console.WriteLine("\tCanceledTasksCount: " + res.CanceledTasksCount);
            Console.WriteLine("\tLongestTask: " + res.LongestTask);
            Console.WriteLine("\tLastProjectUserTasks: " + res.LastProjectUserTasks);
        }

        //Получить следующую структуру(передать Id пользователя в параметры) :
        //User
        //Последний проект пользователя(по дате создания)
        //Общее кол-во тасков под последним проектом
        //Общее кол-во незавершенных или отмененных тасков для пользователя
        //Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)
        //P.S. - в данном случае, статус таска не имеет значения, фильтруем только по дате.
        public (User User, Project LastProject, int CanceledTasksCount, RawTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        {
            var user = projectsService.GetUserByIdAsync(id).Result;
            var lastProject = new Project(projectsService.GetProjectsAsync().Result.OrderBy(x => x.CreatedAt).LastOrDefault());
            var tasks = projectsService.GetTasksAsync().Result.Where(t => t.PerformerId == id);

            return
                (
                    User: new User(user),
                    LastProject: lastProject,
                    CanceledTasksCount: tasks.Join(states, x => x.StateId, y => y.Id, (x,y) => new Task(x) { State = y })
                        .Count(x => x.State.Value == "Canceled" || x.State.Value == "Started"),
                    LongestTask: tasks.OrderBy(x => x.FinishedAt - x.CreatedAt).FirstOrDefault(),
                    LastProjectUserTasks: tasks.Count(x => x.ProjectId == lastProject.Id)
                );
        }
        #endregion

        #region ProjectInfoById
        private void ShowProjectInfoById()
        {
            TypeInfo("Enter ProjectId");
            var res = GetProjectInfoById(GetId());
            if (res.Project == null)
            {
                TypeInfo("Nothing Found");
                return;
            }
            TypeInfo("Result:");
            Console.WriteLine("\tProject: " + res.Project.ToString());
            Console.WriteLine("\tLongestTask: " + res.LongestTask.ToString());
            Console.WriteLine("\tShortestTask: " + res.ShortestTask.ToString());
            if (res.UsersCount != -1)
                Console.WriteLine("\tUsersCount: " + res.UsersCount);
        }
        //Получить следующую структуру(передать Id проекта в параметры) :
        //Проект
        //Самый длинный таск проекта(по описанию)
        //Самый короткий таск проекта(по имени)
        //Общее кол-во пользователей в команде проекта, где или описание проекта > 25 символов или кол-во тасков < 3
        public (Project Project, RawTask LongestTask, RawTask ShortestTask, int UsersCount) GetProjectInfoById(int id)
        {
            var project = projectsService.GetProject(id);
            var projectTasks = projectsService.GetTasksAsync().Result.Where(x => x.ProjectId == project.Id);
            var users = projectsService.GetUsersAsync().Result;

            return
                (
                    Project: project,
                    LongestTask: projectTasks.OrderByDescending(t => t.Description).FirstOrDefault(),
                    ShortestTask: projectTasks.OrderBy(t => t.Name).FirstOrDefault(),
                    UsersCount: (projectTasks.Count() < 3 || project.Description.Length > 25 ? 
                                                users.Where(x => x.TeamId == project.Team.Id).Count() : -1)
                );
        }
        #endregion
    }
}
